
package com.example.khalifa.photoshare;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Main extends AppCompatActivity
{
    public Connection connection;
    User user;
    Button login,register;
    TextView email,password;
    ProgressBar progressBar;
    public Context context;
    public Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.activity= this;
        connection = new Connection();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
         login = (Button) findViewById(R.id.LoginButton);
        register = (Button) findViewById(R.id.RegisterButton);
        email = (TextView) findViewById(R.id.Email);
        password = (TextView) findViewById(R.id.Password);
        user= new User();
        login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (email.getText().length() > 0 && password.getText().length() > 0 )
                {
                    if( !Utlities.isEmailValid(email.getText().toString()))
                    {
                        Toast.makeText(getApplicationContext(),"Please enter a valid email !",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {

                        progressBar.setVisibility(ProgressBar.VISIBLE);
                user.setEmail(email.getText().toString());
                user.setPassword(password.getText().toString());
                task t = new task();
                t.execute();

                    }



                }else
                {
                 Toast.makeText(getApplicationContext(),"all fields are required !",Toast.LENGTH_SHORT).show();
                }

            }


        });

    register.setOnClickListener(new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            Intent i  = new Intent(v.getContext(),registerActivity.class);
            startActivity(i);

        }
    });
    }


 public class task extends AsyncTask<String,String,String>
 {
     @Override
     protected String doInBackground(String... params)
     {

         if(user.login())
         {
             activity.runOnUiThread(new Runnable() {
                 public void run()
                 {
                     progressBar.setVisibility(ProgressBar.INVISIBLE);
                     Toast.makeText(getApplicationContext(),"successfully loged in ",Toast.LENGTH_LONG).show();
                     Intent intent = new Intent(activity, home.class);
                     intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                     intent.putExtra("username",user.getUsername());
                     activity.getApplicationContext().startActivity(intent);
                 }
             });





         }else
         {

             activity.runOnUiThread(new Runnable() {
                 public void run() {
                     progressBar.setVisibility(ProgressBar.INVISIBLE);
                     Toast.makeText(getApplicationContext(), "failed to login check your email and password ! ", Toast.LENGTH_LONG).show();
                 }
             });


         }
         return null;
     }
 }

}
