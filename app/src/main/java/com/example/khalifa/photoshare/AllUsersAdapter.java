package com.example.khalifa.photoshare;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by khalifa on 10/24/2016.
 */

public class AllUsersAdapter extends ArrayAdapter<User> {
   Context context;
    List<User> users;

    public AllUsersAdapter(Context context, List<User> objects) {
        super(context, R.layout.user_view ,objects);
        this.context = context;
    users = objects;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        //prepare or get things ready to render
        LayoutInflater inflater = LayoutInflater.from(context);
        //customize 1 item view
        convertView = inflater.inflate(R.layout.user_view, parent, false);
        TextView userName = (TextView) convertView.findViewById(R.id.UserName);
        userName.setText(users.get(position).getUsername());
        return convertView;
    }
}