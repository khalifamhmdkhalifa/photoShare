
package com.example.khalifa.photoshare;

/**
 * Created by khalifa on 10/23/2016.
 */import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class PagerAdapter extends FragmentStatePagerAdapter
{
    String username;
    int mNumOfTabs;
    public PagerAdapter(FragmentManager fm, int NumOfTabs,String username)
    {
        super(fm);
        this.username = username;
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                AllUsers tab1 = new AllUsers( );
                tab1.setUsername(username);
                return tab1;
            case 1:
                 MyImages tab2 = new MyImages();
                tab2.setUsername(username);
                tab2.setImages_username(username);
                return tab2;
            case 2:
                MyImages tab3 = new MyImages();
                tab3.setUsername(username);
                return tab3;
            case 3:
                about about =  new about();
                return about;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}