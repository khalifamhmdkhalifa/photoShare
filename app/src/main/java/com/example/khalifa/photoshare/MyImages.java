package com.example.khalifa.photoshare;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by khalifa on 10/23/2016.
 */

public class MyImages  extends Fragment
{
    GridView imagesView;
    ImagesAdapter adapter;
    List<ImageObject> allImages;
    Connection connection;
    Activity activity;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getImages_username() {
        return images_username;
    }

    public void setImages_username(String images_username)
    {
        this.images_username = images_username;
    }
    public String images_username;

    String username;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.my_images, container, false);
        imagesView = (GridView) v.findViewById(R.id.myImagesView);
        allImages  =new ArrayList<ImageObject>();

        if(savedInstanceState != null)
        {
            username = savedInstanceState.getString("username");
            images_username = savedInstanceState.getString("images_username");
        Log.e("errrorrr","imagesusername : "+images_username+"  username= " +username);

        }



        adapter = new ImagesAdapter(getContext(),allImages);
        imagesView.setAdapter(adapter);

        connection =new Connection();
        Task task = new Task();
        task.execute();
        activity = this.getActivity();

    imagesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
                    Intent i  = new Intent(view.getContext(),Full_Image.class);
                    i.putExtra("username",username);
                    i.putExtra("image",allImages.get(position));
            view.getContext().startActivity(i);
        }
    });



        return v;


    }


void loadImages()
{

    if (images_username == null)
    {
        connection.database.getReference("images").orderByKey().addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                try
                {
                     ImageObject i = new ImageObject();
                    i.setUsername(dataSnapshot.child("username").getValue(String.class));
                    i.setUri(dataSnapshot.child("image").getValue(String.class));
                   i.setId(dataSnapshot.getKey());
                    //i.setBitmap(u.getimage(activity.getApplicationContext(),i.getUri()));
                  if(! allImages.contains(i))
                  {
                      allImages.add(i);
                    adapter.notifyDataSetChanged();
                  }

                } catch (Exception e)
                {

                    Log.e("errooorrr", "my error " + e.toString());

                    e.printStackTrace();
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {

 loadImages();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot)
            {
                try
                {
                   // progressBar.setVisibility(ProgressBar.VISIBLE);
                    ImageObject i = new ImageObject();
                    i.setUsername(dataSnapshot.child("username").getValue(String.class));
                    i.setUri(dataSnapshot.child("image").getValue(String.class));
                    i.setId(dataSnapshot.getKey());
                    //i.setBitmap(u.getimage(activity.getApplicationContext(),i.getUri()));
                    allImages.remove(i);
                    adapter.notifyDataSetChanged();
                    //progressBar.setVisibility(ProgressBar.INVISIBLE);
                } catch (Exception e)
                {
                    Log.e("errooorrr", "my error " + e.toString());

                    e.printStackTrace();
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
 } else
    {
        connection.database.getReference("images").orderByChild("username").equalTo(images_username).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                try
                {
                    //progressBar.setVisibility(ProgressBar.VISIBLE);
                    ImageObject i = new ImageObject();
                    i.setUsername(dataSnapshot.child("username").getValue(String.class));
                    i.setUri(dataSnapshot.child("image").getValue(String.class));
                  i.setId(dataSnapshot.getKey());
                    //i.setBitmap(u.getimage(activity.getApplicationContext(),i.getUri()));
                    if(! allImages.contains(i))
                    {
                        allImages.add(i);
                        adapter.notifyDataSetChanged();
                    }
                    //progressBar.setVisibility(ProgressBar.INVISIBLE);
                } catch (Exception e)
                {
                    Log.e("errooorrr", "my error " + e.toString());

                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s)
            {
loadImages();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot)
            {
                try
                {
                   // progressBar.setVisibility(ProgressBar.VISIBLE);
                    ImageObject i = new ImageObject();
                    i.setUsername(dataSnapshot.child("username").getValue(String.class));
                    i.setUri(dataSnapshot.child("image").getValue(String.class));
                    i.setId(dataSnapshot.getKey());
                    //i.setBitmap(u.getimage(activity.getApplicationContext(),i.getUri()));
                    allImages.remove(i);
                adapter.notifyDataSetChanged();
                    //progressBar.setVisibility(ProgressBar.INVISIBLE);
                } catch (Exception e)
                {
                    Log.e("errooorrr", "my error " + e.toString());

                    e.printStackTrace();
                }







            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}


    @Override
    public void onResume()
    {
        super.onResume();





/*
        Task task = new Task();
        task.execute();*/

    }

    class Task extends AsyncTask<String,String,String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            loadImages();
            return null;
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("username", username);
        outState.putString("images_username",images_username);

    }



}


