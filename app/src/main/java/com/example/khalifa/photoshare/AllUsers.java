
package com.example.khalifa.photoshare;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khalifa on 10/23/2016.
 */

public class AllUsers extends Fragment
{
    List<User> users;
    ListView usersView;
    AllUsersAdapter adapter;
    Connection connection;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String username;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        connection = new Connection();

        View v =inflater.inflate(R.layout.all_users, container, false);
                usersView = (ListView)v.findViewById(R.id.AllUsers);
        users = new ArrayList<User>();
        adapter = new AllUsersAdapter(getContext(),users);
        usersView.setAdapter(adapter);
        usersView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent i  = new Intent(view.getContext(),UserImages.class);
                i.putExtra("username",username);
                i.putExtra("images_username",users.get(position).getUsername());
                view.getContext().startActivity(i);
            }
        });
        connection.database.getReference("users").orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
               users.clear();
                for (DataSnapshot ss: dataSnapshot.getChildren())
                {
                    users.add(ss.getValue(User.class));
                    Log.e("errooorrr",ss.child("username").getValue().toString());
                }
                adapter.notifyDataSetChanged();

            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e("errooorrr", databaseError.getMessage());
            }
        });
        return v;
    }






}
