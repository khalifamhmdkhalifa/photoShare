package com.example.khalifa.photoshare;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.Toast;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import static android.R.attr.bitmap;
import static com.example.khalifa.photoshare.R.drawable.user;


/**
 * Created by khalifa on 10/23/2016.
 */


public class home extends AppCompatActivity
{

    Bitmap bm;
    Activity activity;
    //String username;
    User user;
    public static ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        user = new User();
        if(savedInstanceState ==null)
        {
            user.setUsername((String) getIntent().getExtras().get("username"));
        }else
        {
            user.setUsername(savedInstanceState.getString("username"));
        }
        activity =this;
        progressBar = (ProgressBar) findViewById(R.id.uploadinbar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("all Users").setIcon(R.drawable.users));
        tabLayout.addTab(tabLayout.newTab().setText("My Images").setIcon(R.drawable.myimages));
        tabLayout.addTab(tabLayout.newTab().setText("All Images").setIcon(R.drawable.allimages));
        tabLayout.addTab(tabLayout.newTab().setText("About").setIcon(R.drawable.about2));
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount(),user.getUsername());
        viewPager.setAdapter(adapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener()
                {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
                    {

                    }

                    @Override
                    public void onPageSelected(int position)
                    {
                        tabLayout.getTabAt(position).select();
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                }

        );


    }

@Override
    public boolean onCreateOptionsMenu(Menu menu)
{
    getMenuInflater().inflate(R.menu.menu_main, menu);
return true;
}
@Override
public boolean onOptionsItemSelected(MenuItem menuItem)
{
int id =menuItem.getItemId();

switch (id)
    {
        case R.id.share:
        {
            if(!user.loading) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }else{
                Toast.makeText(getApplicationContext(),"please wait till the current operation is done.",Toast.LENGTH_SHORT).show();

            }
                return true;
        }


    }

return  super.onOptionsItemSelected(menuItem);

}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1 && resultCode == RESULT_OK && data !=null)
        {
            Log.e("erorrrrrrrrrrrrrrrr","first start");
            Uri selectedimage = data.getData();
            try
            {
                 bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedimage);
               home.progressBar.setVisibility(ProgressBar.VISIBLE);
                task task = new task();
                task.execute();
            }catch (Exception e)
            {
                e.printStackTrace();
                Log.e("erorrrrrrrrrrrrrrrr", e.getMessage());
            }


        }



        super.onActivityResult(requestCode, resultCode, data);
    }




    public class task extends AsyncTask<String,String,String>
    {
        @Override
        protected String doInBackground(String... params)
        {

            if(user.uploadImage(bm))
            {
                activity.runOnUiThread(new Runnable() {
                    public void run()
                    {
                        home.progressBar.setVisibility(ProgressBar.INVISIBLE);
                        Toast.makeText(getApplicationContext(),"successfully uploaded ! ",Toast.LENGTH_LONG).show();

                    }
                });




            }else
            {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "failed to upload the image ! ", Toast.LENGTH_LONG).show();
                    }
                });


            }
            return null;
        }
    }



    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("username", user.getUsername());
    }


}
