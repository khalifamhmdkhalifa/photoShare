
package com.example.khalifa.photoshare;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;

/**
 * Created by khalifa on 10/22/2016.
 */

public class Connection
{
    FirebaseDatabase database;
    boolean loading;
    public Connection()
    {
        try
        {
            database = FirebaseDatabase.getInstance();
           }catch (Exception e)
        {
            e.printStackTrace();

        }
    }

    public  StorageReference getStorageReference()
    {
        try
        {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        return storage.getReferenceFromUrl("gs://photoshare-98ea3.appspot.com");
    }catch (Exception e)
    {
        Log.e("errrror","null ref"+e.getMessage());
        e.printStackTrace();
    return null;
    }
    }
    public DatabaseReference getRefrernce(String reference)
    { try
    {
        return database.getReference(reference);
    }catch (Exception e)
    {
        e.printStackTrace();
    return null;
    }
    }
boolean done;
    public boolean AddToDatabase(DatabaseReference reference,Object value )
    {
    loading = true;
        done =false;
        reference.push().setValue(value).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
            done =  task.isSuccessful();
                loading=false;
            }
        });
        while (loading)
        {
            //busy wait
        }
           return done;
    }

    public boolean updateToDatabase(DatabaseReference reference,Object value )
    {
        try
        {
            reference.setValue(value);
            return true;
        }catch (DatabaseException e)
        {
            e.printStackTrace();
            return false;
        }

    }




}
