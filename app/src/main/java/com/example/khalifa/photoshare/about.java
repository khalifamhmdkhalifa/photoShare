package com.example.khalifa.photoshare;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by khalifa on 10/27/2016.
 */

public class about extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View v =inflater.inflate(R.layout.about, container, false);
        TextView tv = (TextView)v.findViewById(R.id.text);
        Linkify.addLinks(tv, Linkify.ALL);
        return v;
    }

}
