package com.example.khalifa.photoshare;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;

/**
 * Created by khalifa on 10/25/2016.
 */

public class ImageObject implements Serializable
{
    String uri;
    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    Bitmap bitmap;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    String username;

private long likes;
    private boolean loading;

    public long numberOfikes()
    {
            Connection connection = new Connection();
        loading=true;
        likes = 0;
        connection.database.getReference("likes/"+id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                try
                {
                    likes = dataSnapshot.getChildrenCount();
                } catch (Exception e)
                {

                    Log.e("errooorrr", e.getMessage());
                    e.printStackTrace();
                }
                loading =false;
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                loading=false;
                Log.e("errooorrr", databaseError.getMessage());
            }
        });

        while (loading)
        {}

        return likes;
    }




}
