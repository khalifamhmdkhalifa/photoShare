package com.example.khalifa.photoshare;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;

/**
 * Created by khalifa on 10/26/2016.
 */

public class Full_Image extends AppCompatActivity
{
    TextView image_user,title,likes;
    Button delete;
    ImageView imageView, likeIcon;
    ImageObject myImage;
    ProgressBar progressBar;
    Activity activity;
    Boolean liked = false;
    User user;
    long numberoflikes;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image);
        activity =this;

        delete = (Button) findViewById(R.id.Delete);
        progressBar  =(ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        likeIcon = (ImageView) findViewById(R.id.Like_Icon);
        likes = (TextView) findViewById(R.id.likes_number);

        if(savedInstanceState ==null)
         { user = new User();
            user.setUsername((String) getIntent().getExtras().get("username"));
        }else
        {
            user = (User) savedInstanceState.getSerializable("user");
        }
        imageView = (ImageView) findViewById(R.id.my_fullimage);
        image_user = (TextView) findViewById(R.id.User);
        myImage = (ImageObject) getIntent().getExtras().get("image");
       if(user.getUsername().equals(myImage.getUsername()))
        {
            delete.setVisibility(View.VISIBLE);
       delete.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) 
           {
              // progressBar.setVisibility(View.VISIBLE);
               delete_task d = new delete_task();
               d.execute();
           }
       });
        }
        image_user.setText(myImage.getUsername());
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {
                likeIcon.setImageResource(R.drawable.red);
                return false;
            }
        });

        likeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               activity.runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       if(!liked)
                       {
                           likeIcon.setImageResource(R.drawable.red);
                           user.like(myImage);
                           liked = true;

                           likes.setText(String.valueOf( Integer.valueOf(likes.getText().toString()) +1));


                       }else
                       {
                           likeIcon.setImageResource(R.drawable.empty_heart);
                           user.dislike(myImage);
                           liked = false;
                           likes.setText(String.valueOf( Integer.valueOf(likes.getText().toString()) -1));
                       }


                   }
               });

                }
        });

        image_user.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i  = new Intent(v.getContext(),UserImages.class);
                i.putExtra("username",user.getUsername());
                i.putExtra("images_username",myImage.getUsername());
                v.getContext().startActivity(i);
            }
        });

        load_task task =  new load_task();
        task.execute();
    }








void loadImage()
{

    activity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            progressBar.setVisibility(ProgressBar.VISIBLE);
        }
    });

    int width= getResources().getDisplayMetrics().widthPixels;
    int height = getResources().getDisplayMetrics().heightPixels;
    imageView.setMaxHeight(height /2);
    Log.e("errror","image uri : "+myImage.getUri());
    liked = user.isLiked(myImage);
    numberoflikes = myImage.numberOfikes();
    activity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            com.squareup.picasso.Picasso
                    .with(getApplicationContext())
                    .load(myImage.getUri())
                    .into(imageView, new Callback()
                    {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }

                        @Override
                        public void onError()
                        {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"Couldn't load the image",Toast.LENGTH_SHORT).show();
                        }
                    });


            likes.setText(String.valueOf(numberoflikes));
            if(liked)
            {
                likeIcon.setImageResource(R.drawable.red);
            }else
            {
                likeIcon.setImageResource(R.drawable.empty_heart);
            }


        }
    });


}



class delete_task extends AsyncTask<String,String,String>{


    @Override
    protected String doInBackground(String... params) {


        final String message;
        if(user.deleteImage(myImage))
        {
            message = "successfully deleted" ;
             }else
        {

            message = "sorry couldn't delete the image check your connection !" ;

        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                Toast.makeText(activity.getApplicationContext(),message,Toast.LENGTH_SHORT).show();
               // progressBar.setVisibility(View.INVISIBLE);
                Intent i  = new Intent(activity.getApplicationContext(),home.class);
                i.putExtra("username",user.getUsername());
                activity.startActivity(i);

            }
        });


        return null;
    }
}




    class load_task extends AsyncTask<ProgressBar,ProgressBar,ProgressBar>
    {
        @Override
        protected ProgressBar doInBackground(ProgressBar... params) {
            loadImage();
            return null;
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("user", user);
    }




}