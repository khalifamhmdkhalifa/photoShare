package com.example.khalifa.photoshare;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by khalifa on 10/25/2016.
 */

public class ImagesAdapter extends ArrayAdapter<ImageObject>
{
    Context context;
    List<ImageObject> imageObjects;
    public ImagesAdapter(Context context, List<ImageObject> objects)
    {
        super(context, R.layout.image_view ,objects);
        this.context = context;
        imageObjects = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        //prepare or get things ready to render
        LayoutInflater inflater = LayoutInflater.from(context);
        //customize 1 item view
        convertView = inflater.inflate(R.layout.image_view, parent, false);
        ImageView iv = (ImageView) convertView.findViewById(R.id.Image);
        final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.VISIBLE);
        try
        {
            int width= context.getResources().getDisplayMetrics().widthPixels;
           // int height =  context.getResources().getDisplayMetrics().heightPixels;

            iv.setMaxWidth(width/2);
            com.squareup.picasso.Picasso
                    .with(context)
                    .load(imageObjects.get(position).getUri())
                    .centerCrop().resize(width/2,width/2)
                    .into(iv, new Callback() {
                        @Override
                        public void onSuccess()
                        {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                        @Override
                        public void onError()
                        {

                        }
                    });



        }catch (Exception e)
        {
            Log.e("erooooor",e.getMessage());
        }

        return convertView;
    }









}

