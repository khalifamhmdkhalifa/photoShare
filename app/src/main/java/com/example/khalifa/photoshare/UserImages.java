package com.example.khalifa.photoshare;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by khalifa on 10/26/2016.
 */

public class UserImages extends AppCompatActivity
{
    String username;
    FragmentActivity fm;
    String images_username;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_images);
        Log.e("errrro","started");
        username = (String) getIntent().getExtras().get("username");
        images_username = (String) getIntent().getExtras().get("images_username");
        final ViewPager viewPager = (ViewPager) findViewById(R.id.mypager);
        Log.e("errrro","before set adapter");
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position)
            {
                Log.e("errrro","item got");
                MyImages fm = new MyImages();
                fm.setUsername(username);
                fm.setImages_username(images_username);
                return fm;
            }

            @Override
            public int getCount() {
                return 1;
            }
        }
        );
        Log.e("errrro","before try");
        try
        {
            viewPager.setCurrentItem(0);
        }catch (Exception e)
        {
            Log.e("errrro","exception " +e.toString());
        }
        Log.e("errrro","after try");

    }




}
