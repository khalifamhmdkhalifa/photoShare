package com.example.khalifa.photoshare;

import android.graphics.Bitmap;
import android.icu.text.LocaleDisplayNames;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by khalifa on 10/22/2016.
 */

public class User implements Serializable {
    String username;
    private Connection connection;
    protected boolean loading = false;

    public User()
    {
        connection = new Connection();
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String password;
    String email;
    int id;






public Boolean login()
{
    loading=true;

    connection.database.getReference("users").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            if(dataSnapshot.getValue() !=null)
            {
            try
            {

                if(dataSnapshot.getChildren().iterator().hasNext() )
                {
                    Log.i("TAG" , "retrived");
                    User u = dataSnapshot.getChildren().iterator().next().getValue(User.class);
                   Log.i("TAG" , u.getEmail());
                    if(u.getPassword().equals(password))
                    {
                    email = u.getEmail();
                    username = u.getUsername();
                    password  = u.getPassword();
                    }else
                    {
                        email=null;
                    }

                }else
                {
                    email=null;

                }

loading=false;
            } catch (Exception e)
            {
                email=null;
                loading=false;
                Log.e("errooorrr", e.getMessage());
                e.printStackTrace();

            }
            }else{
                loading=false;
                email = null;

            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError)
        {
            email = null;
loading=false;
            Log.e("errooorrr", databaseError.getMessage());
        }
    });

    while (loading)
    {
        try
        {
        Thread.currentThread().join(500);
    } catch (InterruptedException e)
        {
    e.printStackTrace();
}

    }

return (email != null);
}






    public int register()
    {
        if(!checkEmail())
        {
            return -1;
        }
        if(! checkUsername())
        {
            return -2;
        }

found = false;
        HashMap<String, Object> result = new HashMap<>();
        result.put("username", username);
        result.put("password", password);
        result.put("email", email);
            if(connection.AddToDatabase(connection.database.getReference("users"),result))
        {
            return 1;
        }

        return 0;
    }



















protected boolean found;
private boolean checkEmail()
{
    found=false;
    loading=true;
    connection.database.getReference("users").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            try
            {

                if(dataSnapshot.getValue()!=null && dataSnapshot.getChildren().iterator().hasNext() )
                {
                    Log.i("TAG" , "retrived");
                    User u = dataSnapshot.getChildren().iterator().next().getValue(User.class);
                    Log.i("TAG" , u.getEmail());
                    found =true;
                }

            } catch (Exception e)
            {

                Log.e("errooorrr", e.getMessage());
                e.printStackTrace();
            }
        loading =false;
        }

        @Override
        public void onCancelled(DatabaseError databaseError)
        {
            loading=false;
            Log.e("errooorrr", databaseError.getMessage());
        }
    });

    while (loading)
    {}

return !found;
}


    private boolean checkUsername()
    {
        found=false;
        loading=true;
        connection.database.getReference("users").orderByChild("username").equalTo(username).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                try
                {

                    if(dataSnapshot.getValue()!=null&&dataSnapshot.getChildren().iterator().hasNext() )
                    {

                        found =true;
                    }

                } catch (Exception e)
                {

                    Log.e("errooorrr", e.getMessage());
                    e.printStackTrace();
                }
                loading =false;
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                loading=false;
                Log.e("errooorrr", databaseError.getMessage());
            }
        });

        while (loading)
        {}
        return !found;
    }



















    boolean uploadImage(Bitmap bitmap) {
        if (username == null)
        {
            username = "khalifa";
        }
        Log.e("erorrrrrrrrrrrrrrrr", "start");
try {
        HashMap<String, String> map = new HashMap<String, String>();
        DatabaseReference ref = connection.database.getReference("images").push();
        final String key = ref.getKey();
        map.put("image", key);
        map.put("username", this.username);
        ref.setValue(map);
    Log.e("erorrrrrrrrrrrrrrrr", "first push done !");
        found = false;
        loading = true;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

    UploadTask uploadTask = connection.getStorageReference().child("images/" + key + ".jpeg").putBytes(data);

    Log.e("erorrrrrrrrrrrrrrrr", "get storage ref done !");
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.e("erorrrrrrrrrrrrrrrr", "failure");
                loading = false;

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
            {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                HashMap<String, String> map = new HashMap<String, String>();
                connection.database.getReference("images/" + key+"/image").setValue(downloadUrl.toString());
                found = true;
                loading = false;
                Log.e("erorrrrrrrrrrrrrrrr", "success");
            }

        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                home.progressBar.setProgress((int)progress);
                Log.e("erorrrrrrrrrrrrrrrrrr", "Upload is " + progress + "% done");
            }
        });




}catch (Exception e)
{
    e.printStackTrace();
Log.e("erorrrrrrrrrrrrrrrr", e.getMessage());
loading= false;
}





        while (loading)
    {
     /*   try {
            Thread.currentThread().join(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e("erorrrrrrrrrrrrrrrr", e.getMessage());
        }
    */
    }
    return found;

    }
    boolean done;

    public boolean deleteImage(ImageObject myimage)
    {
        loading=true;
        done= false;
        final ImageObject  myImage = myimage;
        Log.e("errror","image id : " + myImage.getId());
                connection.getStorageReference().child("images/" + myImage.getId()+".jpeg").delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            connection.database.getReference("images/"+myImage.getId()).removeValue().addOnSuccessListener(
                                    new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid)
                                        {
                                            done =true;
                                            loading =false;
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e)
                                {

                                    loading = false ;
                                }
                            });
                        }
                        else
                        {

                            loading = false;
                        }

                    }
                });

while (loading){
    try
    {
        Thread.currentThread().join(500);
    }catch (Exception e)
    {
        Log.e("ERRROR",""+e.toString());

    }

}

        return done;
  }



    public Boolean isLiked(ImageObject myImage)
    {
        found=false;
        loading=true;
        connection.database.getReference("likes/"+myImage.getId()+"/"+username).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                try
                {
                 if(dataSnapshot.getValue() !=null )
                 {
                    if (dataSnapshot.getValue().equals("1"))
                    {
                        found  = true;
                    }
                 }

                } catch (Exception e)
                {
                    Log.e("errooorrr", e.getMessage());
                    e.printStackTrace();
                }
                loading =false;
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                loading=false;
                Log.e("errooorrr", databaseError.getMessage());
            }
        });

        while (loading)
        {}

        return found;
    }



    public void like(ImageObject myImage)
    {
        try
        {
            connection.database.getReference("likes/" + myImage.getId() +"/"+username).setValue("1");
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void dislike(ImageObject myImage)
    {
        try
        {
            connection.database.getReference("likes/" + myImage.getId() +"/"+username).removeValue();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }



/*
public Uri getImageUri(String name)
{



}
*/








}

