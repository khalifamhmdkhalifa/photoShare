
package com.example.khalifa.photoshare;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
public class registerActivity extends AppCompatActivity
{
    public Connection connection;
    User user;
    Button login,register;
    TextView email,password,username;
    public Context context;
    public Activity activity;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.activity= this;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        connection = new Connection();
        login = (Button) findViewById(R.id.LoginButton);
        register = (Button) findViewById(R.id.RegisterButton);
        email = (TextView) findViewById(R.id.Email);
        password = (TextView) findViewById(R.id.Password);
        username = (TextView) findViewById(R.id.Username);
        user= new User();
        register.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                progressBar.setVisibility(ProgressBar.VISIBLE);


                if(password.getText().length() < 6 )
                {
                    Toast.makeText(getApplicationContext(), "password  length must be more than 6 !", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
                    return;
                }

                if(username.getText().length() < 4 )
                {
                    Toast.makeText(getApplicationContext(), "user name  length must be more than 4 !", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
                    return;

                }else if( username.getText().toString().contains(" ")){
                    Toast.makeText(getApplicationContext(), "user name musn't contain spaces", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
                    return;

                }

                    if (!Utlities.isEmailValid(email.getText().toString()))
                    {
                        Toast.makeText(getApplicationContext(), "Please enter a valid email !", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                    } else {
                        user.setEmail(email.getText().toString());
                        user.setPassword(password.getText().toString());
                        user.setUsername(username.getText().toString());
                        task t = new task();
                        t.execute();
                    }

                }


        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent i  = new Intent(v.getContext(),Main.class);
                startActivity(i);

            }
        });



    }


    public class task extends AsyncTask<String,String,String>
    {
        @Override
        protected String doInBackground(String... params)
        {
           switch(user.register())
           {
               case 0 :
               {

                   activity.runOnUiThread(new Runnable() {
                       public void run() {
                           Toast.makeText(getApplicationContext(), "failed to register ! ", Toast.LENGTH_LONG).show();
                       }
                   });


                   break;
               }case 1:{
               activity.runOnUiThread(new Runnable() {
                   public void run() {
                       Toast.makeText(getApplicationContext(), "congratulations you succssfully registered ", Toast.LENGTH_LONG).show();
                       activity.startActivity(new Intent(activity.getApplicationContext(),Main.class));
                   }
               });

               break;
           }
               case -1:
               {


                   activity.runOnUiThread(new Runnable() {
                       public void run() {
                           Toast.makeText(getApplicationContext(), "This email is already used !", Toast.LENGTH_LONG).show();
                       }
                   });


                   break;
               } case -2:
           {

               activity.runOnUiThread(new Runnable() {
                   public void run()
                   {
                       Toast.makeText(getApplicationContext(), "This username is already used please select another one !", Toast.LENGTH_LONG).show();
                   }
               });


               break ;
           }

           }


            activity.runOnUiThread(new Runnable() {
                public void run()
                {  progressBar.setVisibility(ProgressBar.INVISIBLE); }
            });

            return null;
        }
    }

}

